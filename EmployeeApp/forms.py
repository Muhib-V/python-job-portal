# forms.py

from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User

class UserRegistrationForm(UserCreationForm):
    email = forms.EmailField(max_length=60, help_text='Required. Add a valid email address')

    class Meta:
        model = User
        fields = ['email', 'username', 'password1', 'password2']

class UserLoginForm(forms.ModelForm):
    class Meta:
        username = forms.CharField(max_length=100)
        password = forms.CharField(widget=forms.PasswordInput)