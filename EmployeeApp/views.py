# from rest_framework import generics
# from .models import MyModel
# from .serializer import MyModelSerializer

# class MyModelListCreate(generics.ListCreateAPIView):
#   queryset = MyModel.objects.all()
#   serializer_class = MyModelSerializer

# views.py
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import UserRegistrationForm, UserLoginForm  # Import both forms together

def home(request):
    # Any logic you want to include in your homepage view
    return render(request, 'home.html')


def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')  # Redirect to login page after successful registration
    else:
        form = UserRegistrationForm()
    return render(request, 'register.html', {'form': form})  # Fix the typo here


# def login(request):
#     if request.method == 'POST':
#         form = UserLoginForm(request.POST)
#         if form.is_valid():
#             # Process login logic here (typically using authentication)
#             return redirect('home')  # Redirect to homepage after successful login
#     else:
#         form = UserLoginForm()
#     return render(request, 'login.html', {'form': form})  # Fix the typo here

def user_login(request):
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)  # Log in the user
                return redirect('home')  # Redirect to home page after successful login
            else:
                # Authentication failed
                error_message = "Invalid username or password."
                return render(request, 'login.html', {'form': form, 'error_message': error_message})
    else:
        form = UserLoginForm()
    return render(request, 'login.html', {'form': form})