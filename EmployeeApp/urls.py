# urls.py

from django.urls import path
from .views import register, login

urlpatterns = [
    path('login/', login, name='login'),
    path('register/', register, name='register'),
    # Define other URLs as needed
]
